#include <stdio.h>
#include "refcounter.h"

static void sub1(int * i) {
    (*i)--;
}

typedef struct ActuallyInt ActuallyInt;
typedef RAGE_HS_COUNTABLE(int, ActuallyInt) RcInt;

int main() {
    int a = 1, b = 1;
    RAGE_HS_COUNT(ra, RcInt, sub1, &a);
    int * i = RAGE_HS_REF(ra);
    if (*i != a) {
        printf("Did not initialise correctly\n");
        return 1;
    }
    RAGE_HS_COUNT(rb, RcInt, sub1, &b);
    RAGE_HS_DEPEND_REF(rb, ra);
    RAGE_HS_DECREMENT_REF(ra);
    if (a != 1 || b != 1) {
        printf("Deallocator invoked early\n");
        return 2;
    }
    RAGE_HS_DECREMENT_REF(rb);
    if (a || b) {
        printf("Deallocator not invoked");
        return 3;
    }
}
